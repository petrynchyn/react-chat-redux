import React, { useState, useCallback } from 'react'
import { connect } from 'react-redux'
import { sendMessage as sendMessageAction } from "../../redux/actions";
import { Container, Form, Button } from 'semantic-ui-react'
import { v4 as uuidv4 } from 'uuid';

const MessageInput = ({ user, sendMessage }) => {

  const [msgText, setMsgText] = useState('')

  const sendMsgHandler = useCallback(
    () => {
      const newMsg = {
        id: uuidv4(),
        text: msgText,
        likes: 0,
        editedAt: '',
        createdAt: new Date().toISOString()
      }
      sendMessage({...user, ...newMsg })
      setMsgText('')
    }, [user, msgText]
  )

  return (
    <Container text  style={{ position:'sticky', bottom: '7em', backgroundColor: 'white' }}>
      <Form onSubmit={sendMsgHandler} widths="equal">
        <Form.Group>
          <Form.TextArea autoFocus value={msgText} onChange={ev => setMsgText(ev.target.value)} placeholder='Message' />
          <Button type='submit' disabled={!msgText.length} secondary>Send</Button>
        </Form.Group>
      </Form>
    </Container>
  )
}

const mapStateToProps = ({chat: {user}}) => ({
  user
})

const mapDispatchToProps = {
  sendMessage: sendMessageAction
}

export default connect(mapStateToProps, mapDispatchToProps)(MessageInput);
