import React, { useState, useCallback } from 'react'
import { connect } from 'react-redux'
import {
  saveMessage as saveMessageAction,
  delMessage as delMessageAction,
  likeMessage as likeMessageAction
} from "./actions";
import { Container, Divider, Segment, Icon, Comment, Confirm, Form, Button, Loader, Modal } from 'semantic-ui-react'
import moment from 'moment';


const MessageList = ({ messages, uId, showLoading, saveMessage, delMessage, likeMessage }) => {
  const [deleteMsgIndex, setDeleteMsgIndex] = useState(null)
  const [editMsgIndex, setEditMsgIndex] = useState(null)
  const [editedMsgText, setEditedMsgText] = useState()
  const [isOpenDialogEditMessage, setIsOpenDialogEditMessage] = useState(false)

  const saveMsgHandler = useCallback(
    () => {
      saveMessage(editMsgIndex, editedMsgText)
      setEditMsgIndex(null)
      setIsOpenDialogEditMessage(false)
    }, [editMsgIndex, editedMsgText]
  )

  const deleteMsgHandler = useCallback(
    () => {
      delMessage(deleteMsgIndex)
      setDeleteMsgIndex(null)
    }, [deleteMsgIndex]
  )

  const editMsgHandler = useCallback(
    (index, text, openDialog = false) => {
      setEditMsgIndex(index)
      setEditedMsgText(text)
      setIsOpenDialogEditMessage(openDialog)
    }
  )

  const cancelEditMsgHandler = useCallback(
    () => {
      setEditMsgIndex(null)
      setIsOpenDialogEditMessage(false)
    }
  )

  const templateFormEditMessage = text => (
    <Form reply onSubmit={saveMsgHandler} onReset={() => cancelEditMsgHandler()}>
      <Form.TextArea autoFocus value={editedMsgText} onChange={ev => setEditedMsgText(ev.target.value)} />
      <Button.Group size="mini" compact floated="right">
        <Button type="reset">Cancel</Button>
        <Button.Or />
        <Button type="submit" disabled={editedMsgText === text} secondary>Save</Button>
      </Button.Group>
    </Form>
  )

  const templateDialogEditMessage = text => (
    <Modal size="mini" dimmer="blurring" closeIcon onClose={() => cancelEditMsgHandler(false)} open={isOpenDialogEditMessage}>
      <Modal.Content style={{ marginBottom: '2rem'}} content={templateFormEditMessage(text)} />
    </Modal>
  )

  const templateActionsMessage = (userId, index, likes, text) => (
    <Comment.Actions>
      <Comment.Action onClick={() => uId !== userId && likeMessage(index)}><Icon name="thumbs up" />{likes}</Comment.Action>
      {uId === userId && <Comment.Action onClick={() => editMsgHandler(index, text)}><Icon name="edit" />Edit</Comment.Action>}
      {uId === userId && <Comment.Action onClick={() => setDeleteMsgIndex(index)} icon='trash'><Icon name="trash" />Delete</Comment.Action>}
      {uId === userId && <Comment.Action onClick={() => editMsgHandler(index, text, true)}><Icon name='setting' /></Comment.Action>}
    </Comment.Actions>
  )

  return (
    <Container loading='true' text style={{ marginTop: '7rem', marginBottom: '2rem'}}>
      {messages.map(({ id, user, userId, avatar, text, createdAt, editedAt, likes = 0 }, index, array) => {
        const curFromNow = moment.utc(createdAt).local().fromNow();
        const prevFromNow = moment.utc(array[index - 1]?.createdAt).local().fromNow();

        return (
          <div key={id}>
            {curFromNow !== prevFromNow
              && <Divider horizontal fitted clearing style={{ 'fontSize': '.7em' }}>{curFromNow}</Divider>
            }
            <Segment
              floated={uId === userId ? 'right' : 'left'}
              style={{ maxWidth: '52%', minWidth: '52%', marginLeft: '1rem', marginRight: '1rem' }}
            >
              <Comment.Group size="small">
                <Comment>
                  {uId !== userId && <Comment.Avatar src={avatar} />}
                  <Comment.Content>
                    <Comment.Author as="a">{user}</Comment.Author>
                    <Comment.Metadata>{editedAt && <Icon name="edit" corner="top right" />}</Comment.Metadata>
                    <Comment.Metadata>{moment.utc(editedAt || createdAt).local().format('llll')}</Comment.Metadata>
                    {editMsgIndex !== index && <Comment.Text>{text}</Comment.Text>}
                    {editMsgIndex === index && (isOpenDialogEditMessage ? templateDialogEditMessage(text) : templateFormEditMessage(text))}
                    {editMsgIndex !== index && templateActionsMessage(userId, index, likes, text)}
                  </Comment.Content>
                </Comment>
              </Comment.Group>
            </Segment>

            <Divider hidden fitted clearing />
          </div>
        )
      })}

      <Confirm
        content='Are you sure want to delete this message?'
        cancelButton='Never mind'
        confirmButton="Let's do it"
        size='mini'
        open={deleteMsgIndex === null ? false : true}
        onCancel={() => setDeleteMsgIndex(null)}
        onConfirm={ deleteMsgHandler }
      />
      
      <Loader active={showLoading}>Loading</Loader>
    </Container>
  )
};

const mapStateToProps = ({chat: {user:{userId}, messages, showLoading}}) => ({
  uId: userId,
  messages,
  showLoading
})

const mapDispatchToProps = {
  saveMessage: saveMessageAction,
  delMessage: delMessageAction,
  likeMessage: likeMessageAction
}

export default connect(mapStateToProps, mapDispatchToProps)(MessageList);
