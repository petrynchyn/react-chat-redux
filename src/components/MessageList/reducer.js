import {
  GET_MESSAGES_STARTED,
  GET_MESSAGES_SUCCESS,
  GET_MESSAGES_FAILURE,
  ADD_MESSAGE,
  SAVE_MESSAGE,
  DEL_MESSAGE,
  LIKE_MESSAGE
} from "./actionTypes";

const initialState = {
  messages: [],
  user: {},
  showLoading: false,
  error_message: ''
};

export default function (state = initialState, action) {
  switch (action.type) {

    case GET_MESSAGES_SUCCESS: {
      return {
        ...state,
        messages: action.payload.messages,
        user: action.payload.messages[0],
        showLoading: false
      }
    }

    case GET_MESSAGES_STARTED: {
      return {
        ...state,
        showLoading: true
      }
    }

    case GET_MESSAGES_FAILURE: {
      return {
        ...state,
        showLoading: false,
        error_message: action.payload.error_message
      }
    }

    case ADD_MESSAGE: {
      return {
        ...state,
        messages: [...state.messages, action.payload.message]
      }
    }

    case SAVE_MESSAGE: {
      state.messages[action.payload.messageIndex].text = action.payload.messageText
      state.messages[action.payload.messageIndex].editedAt = new Date().toISOString()
      return {
        ...state,
        messages: [...state.messages]
      }
    }

    case DEL_MESSAGE: {
      state.messages.splice(action.payload.messageIndex, 1)
      return {
        ...state,
        messages: state.messages
      }
    }

    case LIKE_MESSAGE: {
      state.messages[action.payload.messageIndex].likes = state.messages[action.payload.messageIndex].likes ? 0 : 1
      return {
        ...state,
        messages: [...state.messages]
      }
    }

    default:
      return state
  }
}
