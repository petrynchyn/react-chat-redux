import {
  GET_MESSAGES_STARTED,
  GET_MESSAGES_SUCCESS,
  GET_MESSAGES_FAILURE,
  ADD_MESSAGE,
  SAVE_MESSAGE,
  DEL_MESSAGE,
  LIKE_MESSAGE
} from "./actionTypes"
import { getMessagesFromAPI } from "../../services/messageService"


export const getMessages = () => async dispatch => {
  dispatch(getMessagesStarted());

  try {    
    const messages = await getMessagesFromAPI()

    dispatch(getMessagesSuccess(messages))

  } catch (error) {
    dispatch(getMessagesFailure(error.message))
  }
}

const getMessagesStarted = () => ({
  type: GET_MESSAGES_STARTED
})

const getMessagesSuccess = messages => ({
  type: GET_MESSAGES_SUCCESS,
  payload: { messages }
})

const getMessagesFailure = error_message => ({
  type: GET_MESSAGES_FAILURE,
  payload: {
    error_message
  }
})

export const sendMessage = message => ({
  type: ADD_MESSAGE,
  payload: {
    message
  }
})

export const saveMessage = (messageIndex, messageText) => ({
  type: SAVE_MESSAGE,
  payload: {
    messageIndex,
    messageText
  }
})

export const delMessage = (messageIndex) => ({
  type: DEL_MESSAGE,
  payload: {
    messageIndex
  }
})

export const likeMessage = (messageIndex) => ({
  type: LIKE_MESSAGE,
  payload: {
    messageIndex
  }
})
