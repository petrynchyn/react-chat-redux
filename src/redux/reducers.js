import { combineReducers } from "redux";
import chat from "../components/MessageList/reducer";

export default combineReducers({ chat });