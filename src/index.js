import React from 'react';
import ReactDOM from 'react-dom';
import { Provider as ReduxProvider } from "react-redux";
import reportWebVitals from './reportWebVitals';

import Chat from './containers/Chat/Chat';
import store from './redux/store'
import { getMessages } from "./redux/actions";

import 'semantic-ui-css/semantic.min.css'

store.dispatch(getMessages())

ReactDOM.render(
  <React.StrictMode>
    <ReduxProvider store={store}>
      <Chat />
    </ReduxProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
