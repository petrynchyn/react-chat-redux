import { MESSAGES_ENDPOINT } from "../constants"

export const getMessagesFromAPI = async () => {
  const response = await fetch(MESSAGES_ENDPOINT)

  if (!response.ok) throw new Error(response.status)
  
  return response.json()
};