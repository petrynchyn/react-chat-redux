import React from 'react'
import { Container, Header, Segment } from 'semantic-ui-react'
import Head from "../../components/Head/Head";
import MessageList from "../../components/MessageList/MessageList";
import MessageInput from "../../components/MessageInput/MessageInput";

const Chat = () => {
  return (
    <>
      <Head />

      <MessageList />
      
      <MessageInput />
      
      <Segment inverted vertical style={{ margin: '2em 0em 0em', padding: '2em 0em', position:'sticky', bottom: '0px' }}>
        <Container textAlign="center">
          <Header inverted as="h4" content="&copy; ANDRII PETRYNCHYN" />
        </Container>
      </Segment>
    </>
  )
}

export default Chat
